__version__ = '0.1'
__author__ = 'Salah Eddine Kabbour'

import tkinter as tk
import os
import sys
import datetime
import json

UNIVERSAL = "this should be changed"


class MainPage(tk.Tk):

    def __init__(self, *args, **kwargs):
        # initialization

        tk.Tk.__init__(self, *args, **kwargs)

        path = os.path.dirname(sys.modules['__main__'].__file__)
        # path = os.path.abspath(os.path.join(path, '..'))
        self.path = os.path.abspath(path)
        self.settings_path = os.path.join(self.path, "settings.json")
        self.source_path = ""
        self.start_date = datetime.datetime.fromtimestamp(10)
        self.font = {
            "family": "Verdana",
            "base": 14,
            "title": 18,
            "adjust": 0,
        }
        self._load_settings()
        self._style_frame()
        self._init_components()

    def _style_frame(self):
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        self.geometry("{:d}x{:d}+{:d}+{:d}".format(int(screen_width * 0.5), int(screen_height * 0.5), 100, 100))
        self.configure(bg='#e6e6e6')

    def _init_components(self):
        color_fg = "#262626"
        color_bg = "#e6e6e6"
        color_entry1 = "#ffffb3"
        color_entry2 = "#b3ffcc"
        color_button_backup = "#008000"
        color_button_exit = "#b30000"
        w, h = self.winfo_screenwidth(), self.winfo_screenheight()
        base_h = int(w * 0.025)

        self.size_label = tk.Label(self, text="Size:", fg=color_fg, bg=color_bg)
        self.size_label.place(x=int(w * 0.444), y=int(h * 0.01), width=2 * base_h, height=base_h)

        self.button_plus = tk.Button(self, text="+", command=lambda: self._update_font(2), fg=color_fg, bg=color_bg)
        self.button_plus.place(x=int(w * 0.444), y=int(h * 0.01) + base_h, width=base_h, height=base_h)

        self.button_minus = tk.Button(self, text="-", command=lambda: self._update_font(-2), fg=color_fg, bg=color_bg)
        self.button_minus.place(x=int(w * 0.472), y=int(h * 0.01) + base_h, width=base_h, height=base_h)

        self.title = tk.Label(self, text="Backup and anonymize photos", fg=color_fg, bg=color_bg)
        self.title.config(fg=color_fg, bg=color_bg, borderwidth=1, relief="solid")
        self.title.place(x=int(w * 0.05), y=int(h * 0.01), width=int(w * 0.35), height=2 * base_h)

        self.path_label = tk.Label(self, text="Photos folder path:", fg=color_fg, bg=color_bg, anchor="w")
        self.path_label.place(x=int(w * 0.01), y=int(h * 0.11), width=int(w * 0.48), height=base_h)

        self.path_entry = tk.Entry(self, bg=color_entry1, fg=color_fg)
        self.path_entry.place(x=int(w * 0.01), y=int(h * 0.16), width=int(w * 0.48), height=base_h)
        self.path_entry.insert(0, self.source_path)

        self.date_label = tk.Label(self, text="Backup since the following date (yyyy-mm-dd):", fg=color_fg, bg=color_bg,
                                   anchor="w")
        self.date_label.place(x=int(w * 0.01), y=int(h * 0.21), width=int(w * 0.48), height=base_h)

        self.date_entry_y = tk.Entry(self, bg=color_entry2, fg=color_fg)
        self.date_entry_y.place(x=int(w * 0.01), y=int(h * 0.26), width=int(w * 0.05), height=base_h)
        self.date_entry_y.insert(0, str(self.start_date.year))

        self.dash_1 = tk.Label(self, text="-", fg=color_fg, bg=color_bg)
        self.dash_1.place(x=int(w * 0.06), y=int(h * 0.26), width=int(w * 0.01), height=base_h)

        self.date_entry_m = tk.Entry(self, bg=color_entry2, fg=color_fg)
        self.date_entry_m.place(x=int(w * 0.07), y=int(h * 0.26), width=int(w * 0.025), height=base_h)
        self.date_entry_m.insert(0, str(self.start_date.month))

        self.dash_2 = tk.Label(self, text="-", fg=color_fg, bg=color_bg)
        self.dash_2.place(x=int(w * 0.095), y=int(h * 0.26), width=int(w * 0.01), height=base_h)

        self.date_entry_d = tk.Entry(self, bg=color_entry2, fg=color_fg)
        self.date_entry_d.place(x=int(w * 0.105), y=int(h * 0.26), width=int(w * 0.025), height=base_h)
        self.date_entry_d.insert(0, str(self.start_date.day))

        self.status_label = tk.Label(self, text="", bg=color_bg, borderwidth=1, relief="solid")
        self.status_label.place(x=int(w * 0.01), y=int(h * 0.31), width=int(w * 0.48), height= 2 * base_h)

        self.backup_button = tk.Button(self, text="Backup", command=lambda: self._start_backup(), fg=color_button_backup,
                                       bg=color_bg)
        self.backup_button.place(x=int(w * 0.1), y=int(h * 0.44), width=int(w * 0.1), height=base_h)

        self.exit_button = tk.Button(self, text="Exit", command=lambda: self._exit(), fg=color_button_exit, bg=color_bg)
        self.exit_button.place(x=int(w * 0.3), y=int(h * 0.44), width=int(w * 0.1), height=base_h)

        self._update_font()

    def _update_font(self, adjust_size=0):
        if self.font["adjust"] + self.font["base"] + adjust_size > 0:
            self.font["adjust"] += adjust_size
        self.button_plus.config(font=self._get_font(True, True))
        self.button_minus.config(font=self._get_font(True, True))
        self.title.config(font=self._get_font(True, True))
        normal_size_elements = {self.size_label, self.path_label, self.path_entry, self.size_label, self.date_label,
                                self.date_entry_y, self.dash_1, self.date_entry_m, self.dash_2, self.date_entry_d,
                                self.exit_button, self.backup_button, self.status_label}
        for element in normal_size_elements:
            element.config(font=self._get_font())

    def _update_status(self, i_status, is_alert=False):
        color = "#008000"
        if is_alert:
            color = "#b30000"
        self.status_label.config(text=i_status, fg=color)

    def _get_font(self, is_title=False, is_bold=False):
        font_type = "base"
        if is_title:
            font_type = "title"
        if is_bold:
            return self.font["family"], self.font[font_type] + self.font["adjust"], "bold"
        return self.font["family"], self.font[font_type] + self.font["adjust"]

    def _start_backup(self):
        # self.date_entry_y.insert(1, "1995")
        self._verify_date()
        self.button_plus["state"] = tk.DISABLED
        self.button_minus["state"] = tk.DISABLED
        self.path_entry["state"] = tk.DISABLED
        self.date_entry_y["state"] = tk.DISABLED
        self.date_entry_m["state"] = tk.DISABLED
        self.date_entry_d["state"] = tk.DISABLED
        self.backup_button["state"] = tk.DISABLED
        self.exit_button["state"] = tk.DISABLED
        self.source_path = self.path_entry.get()
        self._save_settings(True)

    def _verify_date(self):
        year_input = self.date_entry_y.get()
        month_input = self.date_entry_m.get()
        day_input = self.date_entry_d.get()
        date_input = year_input + "-" + month_input + "-" + day_input
        if year_input == "" or month_input == "" or day_input == "":
            self._update_status("Error: date entry for is empty, please fill it.".format(date_input), True)
            return False

        try:
            self.start_date = datetime.datetime.strptime(date_input, '%Y-%m-%d')
        except ValueError:
            self._update_status("Error: '{}' is an invalid date.".format(date_input), True)
            return False
        self._update_status("Nice")
        return True

    def _exit(self):
        self.source_path = self.path_entry.get()
        self._save_settings()
        self.quit()

    def _save_settings(self, save_date=False):
        backup_date = self.start_date
        if save_date:
            backup_date = datetime.datetime.today()
        data = {
            "size_adjust": self.font["adjust"],
            "path": self.source_path,
            "last_date": "{}-{}-{}".format(backup_date.year, backup_date.month, backup_date.day)
        }
        text = json.dumps(data, indent=4)
        print(text)
        with open(self.settings_path, 'w') as outfile:
            outfile.write(text)

    def _load_settings(self):
        if not os.path.exists(self.settings_path):
            return
        with open(self.settings_path) as json_file:
            data = json.load(json_file)

            if isinstance(data["size_adjust"], int):
                if data["size_adjust"] + self.font["base"] > 0:
                    self.font["adjust"] = data["size_adjust"]

            if isinstance(data["path"], str):
                self.source_path = data["path"]

            if isinstance(data["last_date"], str):
                try:
                    last = datetime.datetime.strptime(data["last_date"], '%Y-%m-%d')
                    if last + datetime.timedelta(days=1) < datetime.datetime.today():
                        last = last + datetime.timedelta(days=1)
                    self.start_date = last
                except ValueError:
                    print(ValueError)
                    pass


if __name__ == '__main__':
    app = MainPage()
    app.mainloop()

